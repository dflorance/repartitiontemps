# Répartition du temps

## Présentation

Cet outil a été créé pour pouvoir répartir le temps de retranscription de fichiers audio entre plusieurs personnes. Une option est fournie au cas où une personne aurait déjà avancé le travail sur le premier fichier (donc de la minute zéro jusqu'au temps saisi).

## Pourquoi HTML/javascript ?

* j'avais pensé à mettre cet outil en ligne (mais il faut que je me trouve un server)
* facilité de distribution : java peut ne pas être installé sur une machine mais toutes ont un navigateur web
* ça faisait très longtemps que je n'en avais pas fait (et ça se voit au niveau du code ;) )
